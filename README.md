# 2. CKA Pratice Exam Part 2 ACG

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Modifier le déploiement de l'interface Web pour exposer le port HTTP

2. Créer un service pour exposer les pods du déploiement de l'interface Web en externe

3. Augmentez le déploiement de l'interface Web

4. Créer une entrée qui correspond au nouveau service
 
# Contexte

Cette question utilise le `acgk8scluster`. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le hostname/nodenom (c'est-à-dire ssh acgk8s-worker1).

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de quitter et de revenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à `/home/cloud_user/verify.shtout` moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Introduction

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le terrain réel. Examen CKA. Bonne chance!

# Application

je voudrais que tu retranscrive le contenu ci dessous à un laboratoire bien structurée 


Prérequis

Avoir une compréhension de base des concepts Kubernetes
Être familier avec les commandes kubectl
Instructions

## Se connecter au serveur
Connectez-vous au serveur en utilisant les informations d'identification fournies :

```Bash
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Modifier le déploiement web-frontend

Passez au contexte approprié avec kubectl :

```Bash
kubectl config use-context acgk8s
```


Modifiez le déploiement web-frontend dans l'espace de noms web :

```Bash
kubectl edit deployment -n web web-frontend
```


Ajoutez la configuration suivante au fichier YAML pour exposer le port 80 sur les conteneurs NGINX :

```yaml
spec:
  containers:
  - image: nginx:1.14.2
    ports:
    - containerPort: 80
```

une fois édité, appuyer sur "echap" ensuite saisir ":wq" et entrez

>![Alt text](img/image-1.png)

>![Alt text](img/image-2.png)

## Créer un service pour exposer les pods web-frontend

Pour faire simple, nous allons générer un template pour un service de type NodePort, et nous apporterons des modification à partir de là

1. Vérifier que le deploiements présent dans le namespace et qui doit etre exposé vers l'exétérieur est bien présent :

```sh
kubectg get deploy -n web
```


2. Créez un fichier de service nommé web-frontend-svc.yml :

```sh
kubectl expose deployment web-frontend --type=NodePort --port=80 --target-port=80 --name=web-frontend-svc --namespace=web --dry-run=client -o yaml > web-frontend-svc.yaml
```
>![Alt text](img/image-3.png)

Nous obtenons ainsi un fichier à partir du quel nous pouvons plus facilement travailler

```Bash
nano web-frontend-svc.yml
```

Ajoutez la configuration suivante au fichier YAML :

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: web-frontend-svc
  namespace: web
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
    nodePort: 30080
  selector:
    app: web-frontend
  type: NodePort
status:
  loadBalancer: {}
```

3. Créez le service :


```sh
kubectl create -f web-frontend-svc.yml
```


```sh
kubectl get svc -n web #lister les services présent dans le namespace web
```

>![Alt text](img/image-4.png)


4. Vérifiez la disponibilité de Nginx

Dans le navigateur l'on va entrer l'ip public d'un des node suivi du port 30080

>![Alt text](img/image-5.png)


## Augmenter le déploiement web-frontend

Augmentez le nombre de répliques du déploiement :

```sh
kubectl scale deployment web-frontend -n web --replicas=5
```

>![Alt text](img/image-6.png)

Utilisez ce code avec précaution.


## Créer une entrée pour exposer le service

Créez un fichier d'entrée nommé web-frontend-ingress.yml :

Pour cela, nous allons générer un template pour notre objet ingress

```sh
kubectl create ingress web-frontend-ingress --rule="/*=web-frontend-svc:80" --namespace=web --dry-run=client -o yaml > web-frontend-ingress.yml
```

Bash
vi web-frontend-ingress.yml
Utilisez ce code avec précaution.
content_copy
Ajoutez la configuration suivante au fichier YAML :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  creationTimestamp: null
  name: web-frontend-ingress
  namespace: web
spec:
  rules:
  - http:
      paths:
      - backend:
          service:
            name: web-frontend-svc
            port:
              number: 80
        path: /
        pathType: Prefix
```


Créez l'entrée :

```Bash
kubectl create -f web-frontend-ingress.yml
```

>![Alt text](img/image-7.png)

Vous pouvez maintenant accéder à votre application web à l'aide de l'URL suivante :

http://<PUBLIC_IP_ADDRESS>:30080

Félicitations !

Vous avez terminé ce lab et acquis des compétences précieuses pour l'examen Certified Kubernetes Administrator (CKA).

Ressources supplémentaires

## Documentation Kubernetes

github.com/uday559/Kubernetes

